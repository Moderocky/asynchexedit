package com.moderocky.asynchexedit;

import com.moderocky.asynchexedit.action.AsyncSetAction;
import com.moderocky.hex.extension.ExtDescription;
import com.moderocky.hexapi.hextension.Hextension;
import org.bukkit.plugin.java.JavaPlugin;

public class AsyncHexEdit extends Hextension {

    public static AsyncHexEdit asyncHexEdit;

    public AsyncHexEdit(ExtDescription description) {
        super(description);
        asyncHexEdit = this;
    }

    public static AsyncHexEdit getAsyncHexEdit() {
        return asyncHexEdit;
    }

    public static JavaPlugin getJavaPlugin() {
        return getAsyncHexEdit().getPlugin();
    }

    @Override
    public void onEnable() {
        getHexAPI().register(
                new AsyncSetAction()
        );
    }

    @Override
    public void onDisable() {

    }
}
