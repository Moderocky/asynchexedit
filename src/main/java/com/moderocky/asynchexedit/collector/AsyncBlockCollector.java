package com.moderocky.asynchexedit.collector;

import com.moderocky.asynchexedit.AsyncHexEdit;
import com.moderocky.asynchexedit.actor.AsyncBlockChanger;
import com.moderocky.hex.internal.convenience.Utilities;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class AsyncBlockCollector extends Thread {

    private final @NotNull List<Block> blocks = new ArrayList<>();
    private final @NotNull Location[] bounds;
    private Consumer<AsyncBlockCollector> collectorConsumer = null;

    public AsyncBlockCollector(Location[] locations) {
        if (locations == null || locations.length < 2 || locations[0] == null || locations[1] == null)
            throw new IllegalArgumentException();
        bounds = Utilities.correctBounds(new Location[]{locations[0].clone(), locations[1].clone()});
    }

    public boolean hasCollected() {
        return blocks.size() > 0;
    }

    @NotNull
    public List<Block> getBlocks() {
        return blocks;
    }

    public void onCompletion(Consumer<AsyncBlockCollector> consumer) {
        collectorConsumer = consumer;
    }

    @Override
    public void run() {
        blocks.clear();
        int i = 0;
        for (double y = bounds[0].getY(); y <= bounds[1].getY(); y++) {
            for (double x = bounds[0].getX(); x <= bounds[1].getX(); x++) {
                for (double z = bounds[0].getZ(); z <= bounds[1].getZ(); z++) {
                    Location location = new Location(bounds[0].getWorld(), (int) x, (int) y, (int) z);
                    Bukkit.getScheduler().callSyncMethod(AsyncHexEdit.getJavaPlugin(), () -> blocks.add(location.getBlock()));
                    i++;
                    if (i > 500) {
                        i = 0;
                        try {
                            sleep(1);
                        } catch (InterruptedException e) {
                            // We don't need to know if it can't sleep.
                        }
                    }
                }
            }
        }
        try {
            sleep(1);
        } catch (InterruptedException e) {
            // We don't need to know if it can't sleep.
        }
        if (collectorConsumer != null)
            collectorConsumer.accept(this);
    }
}
