package com.moderocky.asynchexedit.actor;

import com.moderocky.asynchexedit.AsyncHexEdit;
import com.moderocky.asynchexedit.collector.AsyncBlockCollector;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class AsyncBlockChanger extends Thread implements Runnable {

    private final @NotNull AsyncBlockCollector collector;
    private ActionType actionType;
    private Material material;
    private BlockData data;
    private Consumer<Block> blockConsumer;
    private Consumer<AsyncBlockChanger> changerConsumer = null;

    public AsyncBlockChanger(@NotNull AsyncBlockCollector collector) {
        this.collector = collector;
    }

    public AsyncBlockChanger setType(Material material) {
        actionType = ActionType.CHANGE_MATERIAL;
        this.material = material;
        return this;
    }

    public AsyncBlockChanger setBlockData(BlockData blockData) {
        actionType = ActionType.CHANGE_DATA;
        this.data = blockData;
        return this;
    }

    @Override
    public void run() {
        if (!collector.hasCollected()) return;
        if (actionType == ActionType.CHANGE_MATERIAL) {
            blockConsumer = block -> block.setType(material);
        } else {
            blockConsumer = block -> block.setBlockData(data);
        }
        accept(collector.getBlocks());
        try {
            sleep(1);
        } catch (InterruptedException e) {
            //
        }
        if (changerConsumer != null)
            changerConsumer.accept(this);
    }

    private void accept(List<Block> blocks) {
        int i = 0;
        for (Block block : new ArrayList<>(blocks)) {
            i++;
            Bukkit.getScheduler().callSyncMethod(AsyncHexEdit.getJavaPlugin(), () -> execute(block));
            if (i > 300) {
                i = 0;
                try {
                    sleep(1);
                } catch (InterruptedException e) {
                    // We don't need to know if it can't sleep.
                }
            }
        }
    }

    private boolean execute(Block block) {
        blockConsumer.accept(block);
        return true;
    }

    protected enum ActionType {
        CHANGE_MATERIAL,
        CHANGE_DATA
    }

    public AsyncBlockChanger onCompletion(Consumer<AsyncBlockChanger> consumer) {
        changerConsumer = consumer;
        return this;
    }

}
