package com.moderocky.asynchexedit.action;

import com.moderocky.asynchexedit.actor.AsyncBlockChanger;
import com.moderocky.asynchexedit.collector.AsyncBlockCollector;
import com.moderocky.hexapi.editor.action.EditorAction;
import com.moderocky.hexapi.editor.argument.ArgBlockData;
import com.moderocky.hexapi.editor.argument.Argument;
import com.moderocky.hexedit.Selection;
import com.moderocky.hexedit.Wand;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.block.data.BlockData;
import org.bukkit.entity.Player;

public class AsyncSetAction implements EditorAction {

    @Override
    public boolean apply(Player player, Wand wand, Selection selection, String... strings) {
        ArgBlockData argBlockData = (ArgBlockData) getArguments()[0];
        BlockData to = argBlockData.serialise(strings[0]);
        AsyncBlockCollector collector = new AsyncBlockCollector(wand.getBounds());
        collector.onCompletion(collector1 -> {
            new AsyncBlockChanger(collector1)
                    .onCompletion(changer -> player.spigot().sendMessage(new ComponentBuilder("")
                            .append("Hex")
                            .color(ChatColor.AQUA)
                            .append("›")
                            .color(ChatColor.DARK_GRAY)
                            .append(" ")
                            .color(ChatColor.WHITE)
                            .append("Action completed successfully!")
                            .color(ChatColor.GRAY)
                            .create()
                    ))
                    .setBlockData(to)
                    .start();
            player.spigot().sendMessage(new ComponentBuilder("")
                    .append("Hex")
                    .color(ChatColor.AQUA)
                    .append("›")
                    .color(ChatColor.DARK_GRAY)
                    .append(" ")
                    .color(ChatColor.WHITE)
                    .append("Blocks collected. Beginning replacement phase.")
                    .color(ChatColor.GRAY)
                    .create()
            );
        });
        collector.start();
        return true;
    }

    @Override
    public boolean apply(Wand wand, Selection selection, String... strings) {
        return true;
    }

    @Override
    public BaseComponent[] getSuccessMessage() {
        return new ComponentBuilder("")
                .append("Hex")
                .color(ChatColor.AQUA)
                .append("›")
                .color(ChatColor.DARK_GRAY)
                .append(" ")
                .color(ChatColor.WHITE)
                .append("Action prepared. Beginning collection phase.")
                .color(ChatColor.GRAY)
                .create();
    }

    @Override
    public boolean requiresSelection() {
        return true;
    }

    @Override
    public EditorAction create() {
        return new AsyncSetAction();
    }

    @Override
    public Argument<?>[] getArguments() {
        return new Argument[]{
                new ArgBlockData()
        };
    }

    @Override
    public String getName() {
        return "aset";
    }

}
